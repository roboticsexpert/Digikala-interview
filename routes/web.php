<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/explore', 'HomeController@explore')->name('explore');
Route::get('/search', 'HomeController@search')->name('search');
Route::get('/register', 'Auth\RegisterController@index')->name('register');
Route::post('/register', 'Auth\RegisterController@create')->name('register.create');

Route::prefix('profile')->group(function () {
    Route::get('/', 'Auth\ProfileController@index')->name('profile');
    Route::post('/update', 'Auth\ProfileController@update')->name('profile.update');
    Route::post('/changePassword', 'Auth\ProfileController@changePassword')->name('profile.changePassword');
    Route::post('/follow/{slug}', 'Auth\ProfileController@follow')->name('profile.follow');

});
Route::get('/{slug}', 'Auth\ProfileController@user')->name('user');

