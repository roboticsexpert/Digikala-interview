#!/usr/bin/env bash
# move project in mantance mode
php artisan down --message='we are deploying' --retry=120;

# install composer packages
composer install;

#npm for compile assets
npm install;
npm run production;

# clear all caches
php artisan cache:clear;
php artisan config:clear;
php artisan view:clear;
php artisan route:clear;


# migrate database
php artisan migrate --force;

# cache config and route
php artisan config:cache;
php artisan route:cache;

# optimize
php artisan optimize;

# restart queues to load new code
php artisan queue:restart

# upping site
php artisan up;
