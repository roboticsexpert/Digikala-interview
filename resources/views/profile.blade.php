@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">User public information</div>

                    <div class="panel-body">
                        <div class="user-info">
                            <div class="image">
                                <img src="{{asset($user->image->url())}}" class="profile-image"/>
                            </div>
                            <div class="info">
                                @foreach(['Name'=>$user->name,'Email'=>$user->email,'Age'=>$user->birthday?$user->birthday->diffInYears(Carbon\Carbon::now()):'Not provided'] as $key=>$value)
                                    <div class="info-row">
                                        <div class="key">
                                            {{$key}}
                                        </div>
                                        <div class="value">
                                            {{$value}}
                                        </div>
                                    </div>
                                @endforeach

                                @if(!$followed)
                                    <form method="POST" action="{{route('profile.follow',['slug'=>$user->slug])}}">
                                        {!! csrf_field() !!}
                                        <input type="submit" value="follow">
                                    </form>
                                @else
                                    <span class="label label-success">Followed</span>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
