@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Home</div>

                <div class="panel-body">
                    This is simple home page ... check navigation for what you want
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
