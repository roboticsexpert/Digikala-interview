@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Explore</div>

                    <div class="panel-body">
                        <div class="search-container">
                            <h1>
                                Search everything you want
                            </h1>
                            <form>
                                <input type="text" name="query" class="form-control"/>
                            </form>
                        </div>
                        @forelse($users as $user)
                            @include('users.row',compact('user'))
                            @empty
                            No row found
                        @endforelse
                        {{$users->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
