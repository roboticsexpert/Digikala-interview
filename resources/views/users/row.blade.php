<a href="{{route('user',['slug'=>$user->slug])}}">
    <div class="user-row">
        <div class="image">
            <img src="{{asset($user->image->url())}}"/>
        </div>
        <div class="information">
            <h4>
                {{$user->name}}
            </h4>
            <h5>
                {{$user->email}}
            </h5>
        </div>
    </div>
</a>