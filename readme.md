# Digikala Review

this project writen for digikala company 


Live at : [Link](http://138.197.67.193/)

-----------------------

## Framwork

I used Laravel 5.4 as my framework.

[Visit Laravel project](https://laravel.com/docs/5.4/)

## Database

I used Mysql 5.7 as my default database but this project support using these database engines like:

- MySQL
- Mariadb
- PostgreSQL
- SQLite
- SQL Server

[More informatio about database](https://laravel.com/docs/5.4/database)

## Cache

Cache can help speed up your application by caching database results, HTML fragments, or anything else that might be expensive to generate.

I used redis as my default cache engine but this project support using these cache engines like:

* Memcached
* Redis

[More informatio about laravel cache](https://laravel.com/docs/5.4/cache)

### What's the usage of cahce in this project?

In this project, you can just cache db queries. for this purpuse i used repository layer and cached every queries until any related model changes (Create,Update,Delete).

**any changes to models should done in repository layer**

i used one package for repository layer in laravel:

[Github for more info](https://github.com/andersao/l5-repository)

Before cache

![befor cache](https://gitlab.com/roboticsexpert/Digikala-interview/raw/master/docs/beforce-cache.png)

After cache

![befor cache](https://gitlab.com/roboticsexpert/Digikala-interview/raw/master/docs/after-cache.png)



## Queue

I used RabbitMq as my default queue provider but this project support using these queue engines:

* RabbitMQ
* Database
* Redis
* Amazon SQS
* Beanstalkd

[More informatio about laravel queue](https://laravel.com/docs/5.4/queue)

### Why using queues in this project?

some of our codes like calling external api or complicated qureies or anything that get more time than a normal http request time , should move to seprated place and done by another process … (queues have wider usage than anything that i tought )

in this project, we should sync our models in elasticsearch or algolia for our searching system … and these actions should dispatch to workers instead of done in user requests … 

laravel not support rabbitmq as built-in features and you should implement that as yourself.

i found one solution for that in github and used that:

[Github url for more information](https://github.com/vyuldashev/laravel-queue-rabbitmq)

## Search

In this project, laravel scout used for handle search system.

I used Elasticsearch as my default search engine but this project support using these search enigines:

* Elasticsearch
* Algolia
* Database

[More informatio about scout](https://laravel.com/docs/5.4/scout)

## Process monitor (Running queue workers)

I used Supervisord for run queue workers and make them allways work.

[More informatio about supervisord](http://supervisord.org/)



## How to run project ?

> cp .env-example .env

fill .env file with you configuration
> composer install 

> php artisan key:generate

> php artisan migrate 

> npm run [dev,production]

for run background jobs you should run this command:
> php artisan queue:work

This project has many dependency for example: database , cache , queue and etc

thus i create docker-compose file for test it in developement environment ...

My docker contain these container for running project:

* Nginx
* PHP-FPM
* Redis
* Mysql
* RabbitMQ
* Supervisord
* Elasticsearch

for get configured environment just install docker and run these command :
> cp .env-example .env

> cd docker

after going to docker folder:

> cp env-example .env

and change anything you want, after that you can run docker by this command: 

> docker-compose run -d php-deploy php-fpm nginx redis rabbitmq elasticsearch kibana php-worker

> docker exec -i -t docker_php_deploy_1 "php artisan key:generate"


## Packages

## Laravel stapler

Stapler-based file upload package for the Laravel framework.

[Github link](https://github.com/CodeSleeve/laravel-stapler)

## Laravel repository

Laravel 5 - Repositories to abstract the database layer

[Github link](https://github.com/andersao/l5-repository)

## Laravel fractal

An easy to use Fractal wrapper built for Laravel applications (**transformer layer**)

[Github link](https://github.com/spatie/laravel-fractal)

## Laravel sluggable

An opinionated package to create slugs for Eloquent models

[Github link](https://github.com/spatie/laravel-sluggable)

## Laravel debugger

Laravel Debugbar (Integrates PHP Debug Bar)

for check ran quries , cache , loaded view , etc

[Github link](https://github.com/barryvdh/laravel-debugbar)

## Laravel scout

Laravel Scout provides a simple, driver based solution for adding full-text search to your Eloquent models.

[laravel scout link](https://laravel.com/docs/5.3/scout)

## Laravel queue rabbitmq

RabbitMQ driver for Laravel Queue

[Github link](https://github.com/vyuldashev/laravel-queue-rabbitmq)

## Laravel scout elastic

Elastic Driver for Laravel Scout

[Github link](https://github.com/ErickTamayo/laravel-scout-elastic)


## Lardock

[Github link](https://github.com/laradock/laradock)

A full PHP development environment for Docker.

# Test

You can run phpunit for test project
but not test case :(


