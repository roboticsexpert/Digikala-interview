<?php

namespace App\Providers;

use App\Contracts\Repositories\UserRepository;
use App\Repositories\Eloquent\UserRepositoryEloquent;
use App\Services\FriendShipService;
use App\Services\RepositoryUserProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use App\Services\UserRegistrationService;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Auth::provider('repository', function ($app, array $config) {
            // Return an instance of Illuminate\Contracts\Auth\UserProvider...
            return new RepositoryUserProvider(app(UserRepository::class));
        });


    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //bind eloquent repository to user repository
        $this->app->bind(UserRepository::class, UserRepositoryEloquent::class);

        $this->app->singleton(UserRegistrationService::class, function ($app) {
            return new UserRegistrationService(app(UserRepository::class));
        });

        $this->app->singleton(FriendShipService::class, function ($app) {
            return new FriendShipService(app(UserRepository::class));
        });
    }


    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            UserRegistrationService::class,
            UserRepository::class,
            RepositoryUserProvider::class,
            FriendShipService::class
        ];
    }
}
