<?php

namespace App\Repositories\Eloquent;

use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Contracts\Repositories\UserRepository;
use App\Models\User;
use App\Validators\UserValidator;
use Prettus\Repository\Events\RepositoryEntityUpdated;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class UserRepositoryEloquent
 * @package namespace App\Repositories\Eloquent;
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepository, CacheableInterface
{
    //add Cacheable repository for cache answer of queries
    use CacheableRepository;

    /**
     * disable presenter (no need)
     * @var bool
     */
    protected $skipPresenter = true;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Specify Model validator
     * @return string
     */
    public function validator()
    {
        return UserValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * return user by slug
     * @param $slug
     * @return User
     */
    public function findBySlug($slug): User
    {
        return $this->findByField('slug', $slug)->first();
    }

    /**
     * Make user follow another user
     * @param User $user
     * @param User $friend
     */
    public function follow(User $user, User $friend)
    {
        //make this user following another
        $user->friends()->syncWithoutDetaching([$friend->id]);
        //dispatch event for flush cache for this model
        event(new RepositoryEntityUpdated($this, $user));
    }

    /**
     * Check user followed another user or nt
     * @param $user_id
     * @param $friend_id
     * @return bool
     */
    public function areUserFollowing($user_id, $friend_id)
    {
        //TODO improve query (for caching i didn't change that ...)
        $user = $this->whereHas('friends', function ($query) use ($friend_id) {
            $query->where('id', $friend_id);
        })->findWhere(['id' => $user_id]);

        if ($user->count() > 0) {
            return true;
        }

        return false;
    }
}
