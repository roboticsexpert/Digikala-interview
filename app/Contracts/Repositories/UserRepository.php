<?php

namespace App\Contracts\Repositories;

use App\Models\User;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRepository
 * @package namespace App\Contracts\Repositories;
 */
interface UserRepository extends RepositoryInterface
{
    public function findBySlug($slug): User;

    public function follow(User $user, User $friend);

    public function areUserFollowing($user_id, $friend_id);
}
