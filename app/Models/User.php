<?php

namespace App\Models;

use Codesleeve\Stapler\ORM\EloquentTrait;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Scout\Searchable;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class User extends Authenticatable implements StaplerableInterface, Transformable
{
    //transform
    use TransformableTrait;
    //trait for notification users like email, sms , etc
    use Notifiable;
    //laravel stapler trait
    use EloquentTrait;

    //make this model sluggable
    use Sluggable;

    //make this model searchable
    use Searchable;

    public function __construct(array $attributes = array())
    {
        $this->hasAttachedFile('image', [
            'styles' => [
                'medium' => '300x300',
                'thumb' => '100x100'
            ]
        ]);

        parent::__construct($attributes);
    }


    /**
     * Return attributes to send to search engine
     * @return array
     */
    public function toSearchableArray()
    {
        //make email and name searchable
        return [
            'name' => $this->name,
            'email' => $this->email
        ];
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'image', 'birthday'
    ];


    protected $dates = ['birthday'];

    public function friends()
    {
        return $this->belongsToMany(User::class, 'friendships', 'user_id', 'friend_id');
    }
}
