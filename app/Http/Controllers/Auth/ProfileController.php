<?php

namespace App\Http\Controllers\Auth;

use App\Contracts\Repositories\UserRepository;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\FriendShipService;
use Illuminate\Http\Request;
use Prettus\Validator\Exceptions\ValidatorException;

class ProfileController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * create controller
     * ProfileController constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->middleware('auth', ['except' => ['user']]);
        $this->userRepository = $userRepository;
    }


    /**
     * Show user public information
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function user($slug, Request $request)
    {
        $user = $this->userRepository->findBySlug($slug);

        $loginUser = $request->user();
        if ($loginUser)
            $followed = $this->userRepository->areUserFollowing($loginUser->id, $user->id);
        else
            $followed = false;

        return view('profile', compact('user', 'followed'));
    }

    /**
     * Show user profile
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $user = $request->user();
        return view('auth.profile', compact('user'));
    }

    /**
     * Update user profile
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {

        $user = $request->user();
        try {
            $this->userRepository->update($request->all(), $user->id);
        } catch (ValidatorException $validatorException) {
            return redirect()->back()->withInput($request->all())->withErrors($validatorException->getMessageBag());
        }

        return redirect()->back();
    }

    /**
     * Change password of user
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changePassword(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|string|min:6|confirmed',
        ]);

        $user = $request->user();

        try {
            $this->userRepository->update($request->all(), $user->id);
        } catch (ValidatorException $validatorException) {
            return redirect()->back()->withInput($request->all())->withErrors($validatorException->getMessageBag());
        }

        return redirect()->back();

    }

    public function following(Request $request)
    {

    }

    public function followed(Request $request)
    {

    }

    public function follow($slug, Request $request)
    {
        /** @var User $user */
        $user = $request->user();

        $this->userRepository->follow($user, $this->userRepository->findBySlug($slug));

        return redirect()->back();
    }
}
