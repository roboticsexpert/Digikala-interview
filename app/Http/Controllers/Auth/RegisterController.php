<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\UserRegistrationService;
use Illuminate\Support\Facades\Auth;
use Prettus\Validator\Exceptions\ValidatorException;

class RegisterController extends Controller
{
    /**
     * @var UserRegistrationService
     */
    private $userRegistrationService;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRegistrationService $userRegistrationService)
    {
        $this->middleware('guest');
        $this->userRegistrationService = $userRegistrationService;
    }

    public function index()
    {
        return view('auth.register');
    }

    public function create(Request $request)
    {
        try {
            $user = $this->userRegistrationService->register($request->all());
        } catch (ValidatorException $validatorException) {
            return redirect()->back()->withInput($request->all())->withErrors($validatorException->getMessageBag());
        }
        Auth::login($user);
        return redirect()->route('home');
    }

}
