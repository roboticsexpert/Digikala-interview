<?php

namespace App\Http\Controllers;

use App\Contracts\Repositories\UserRepository;
use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function explore(UserRepository $userRepository)
    {
        $users = $userRepository->paginate();
        return view('explore', compact('users'));
    }

    public function search(Request $request)
    {
        if ($request->has('query'))
            $users = User::search($request->input('query'))->paginate();
        else
            $users = User::paginate();

        return view('search', compact('users'));

    }
}
