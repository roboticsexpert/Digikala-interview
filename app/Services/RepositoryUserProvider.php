<?php
/**
 * Created by PhpStorm.
 * User: roboticsexpert
 * Date: 9/20/17
 * Time: 2:21 PM
 */

namespace App\Services;


use App\Contracts\Repositories\UserRepository;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Support\Facades\Hash;

class RepositoryUserProvider implements UserProvider
{

    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var
     */
    private $identifier;

    public function __construct(UserRepository $userRepository, $identifier = 'email')
    {
        $this->userRepository = $userRepository;
        $this->identifier = $identifier;
    }

    /**
     * Retrieve a user by their unique identifier.
     *
     * @param  mixed $identifier
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveById($identifier)
    {
        $user = $this->userRepository->find($identifier);
        return $user;
    }

    /**
     * Retrieve a user by their unique identifier and "remember me" token.
     *
     * @param  mixed $identifier
     * @param  string $token
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByToken($identifier, $token)
    {
        return $this->userRepository->findWhere(['id' => $identifier, 'remember_token' => $token])->first();
    }

    /**
     * Update the "remember me" token for the given user in storage.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  string $token
     * @return void
     */
    public function updateRememberToken(Authenticatable $user, $token)
    {
        $user->setRememberToken($token);
    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array $credentials
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        return $this->userRepository->findByField('email', $credentials['email'])->first();
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  array $credentials
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        return $answer= Hash::check($credentials['password'],$user->getAuthPassword());
    }
}