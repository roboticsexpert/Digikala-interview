<?php
/**
 * Created by PhpStorm.
 * User: roboticsexpert
 * Date: 9/20/17
 * Time: 11:45 AM
 */

namespace App\Services;


use App\Contracts\Repositories\UserRepository;
use App\Models\User;
use Prettus\Repository\Events\RepositoryEntityUpdated;

class UserRegistrationService
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function register($data = array()): User
    {

        /** @var User $user */
        $user = $this->userRepository->create(
            $this->preparePassword($data)
        );

        return $user;
    }

    private function preparePassword($date)
    {
        if (isset($date['password']))
            $date['password'] = bcrypt($date['password']);
        return $date;
    }

}