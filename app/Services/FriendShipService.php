<?php
/**
 * Created by PhpStorm.
 * User: roboticsexpert
 * Date: 9/20/17
 * Time: 11:45 AM
 */

namespace App\Services;


use App\Contracts\Repositories\UserRepository;
use App\Models\User;

class FriendShipService
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function areFriend($user_id, $friend_id)
    {
        $user = $this->userRepository->whereHas('friends', function ($query) use ($friend_id) {
            $query->where('id', $friend_id);
        })->findWhere(['id'=>$user_id]);


        //fixme cache this query
        /*$user = User::whereHas('friends', function ($query) use ($friend_id) {
            $query->where('id', $friend_id);
        })->where('id',$user_id)->first();*/

        if($user->count()>0)
        {
            return true;
        }

        return false;
    }

    public function follow($user_id,$friend_id)
    {

    }

}