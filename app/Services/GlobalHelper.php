<?php
/**
 * Created by PhpStorm.
 * User: roboticsexpert
 * Date: 3/9/17
 * Time: 12:52 AM
 */

namespace App\Services;


use Carbon\Carbon;

class GlobalHelper
{
    public static function formatFileSize($fileSize)
    {
        $mb = round($fileSize / (1042 * 1024), 2);
        return $mb . ' MB';
    }

    public static function formatTransactionStatus($status)
    {
        if($status == 'PAID')
            return 'پرداخت شده';
        else if($status == 'UNPAID')
            return 'پرداخت ناموفق';
    }

    public static function slug($string, $separator = '-')
    {
        $_transliteration = array(
            '/ä|æ|ǽ/' => 'ae',
            '/ö|œ/' => 'oe',
            '/ü/' => 'ue',
            '/Ä/' => 'Ae',
            '/Ü/' => 'Ue',
            '/Ö/' => 'Oe',
            '/À|Á|Â|Ã|Å|Ǻ|Ā|Ă|Ą|Ǎ/' => 'A',
            '/à|á|â|ã|å|ǻ|ā|ă|ą|ǎ|ª/' => 'a',
            '/Ç|Ć|Ĉ|Ċ|Č/' => 'C',
            '/ç|ć|ĉ|ċ|č/' => 'c',
            '/Ð|Ď|Đ/' => 'D',
            '/ð|ď|đ/' => 'd',
            '/È|É|Ê|Ë|Ē|Ĕ|Ė|Ę|Ě/' => 'E',
            '/è|é|ê|ë|ē|ĕ|ė|ę|ě/' => 'e',
            '/Ĝ|Ğ|Ġ|Ģ/' => 'G',
            '/ĝ|ğ|ġ|ģ/' => 'g',
            '/Ĥ|Ħ/' => 'H',
            '/ĥ|ħ/' => 'h',
            '/Ì|Í|Î|Ï|Ĩ|Ī|Ĭ|Ǐ|Į|İ/' => 'I',
            '/ì|í|î|ï|ĩ|ī|ĭ|ǐ|į|ı/' => 'i',
            '/Ĵ/' => 'J',
            '/ĵ/' => 'j',
            '/Ķ/' => 'K',
            '/ķ/' => 'k',
            '/Ĺ|Ļ|Ľ|Ŀ|Ł/' => 'L',
            '/ĺ|ļ|ľ|ŀ|ł/' => 'l',
            '/Ñ|Ń|Ņ|Ň/' => 'N',
            '/ñ|ń|ņ|ň|ŉ/' => 'n',
            '/Ò|Ó|Ô|Õ|Ō|Ŏ|Ǒ|Ő|Ơ|Ø|Ǿ/' => 'O',
            '/ò|ó|ô|õ|ō|ŏ|ǒ|ő|ơ|ø|ǿ|º/' => 'o',
            '/Ŕ|Ŗ|Ř/' => 'R',
            '/ŕ|ŗ|ř/' => 'r',
            '/Ś|Ŝ|Ş|Ș|Š/' => 'S',
            '/ś|ŝ|ş|ș|š|ſ/' => 's',
            '/Ţ|Ț|Ť|Ŧ/' => 'T',
            '/ţ|ț|ť|ŧ/' => 't',
            '/Ù|Ú|Û|Ũ|Ū|Ŭ|Ů|Ű|Ų|Ư|Ǔ|Ǖ|Ǘ|Ǚ|Ǜ/' => 'U',
            '/ù|ú|û|ũ|ū|ŭ|ů|ű|ų|ư|ǔ|ǖ|ǘ|ǚ|ǜ/' => 'u',
            '/Ý|Ÿ|Ŷ/' => 'Y',
            '/ý|ÿ|ŷ/' => 'y',
            '/Ŵ/' => 'W',
            '/ŵ/' => 'w',
            '/Ź|Ż|Ž/' => 'Z',
            '/ź|ż|ž/' => 'z',
            '/Æ|Ǽ/' => 'AE',
            '/ß/' => 'ss',
            '/Ĳ/' => 'IJ',
            '/ĳ/' => 'ij',
            '/Œ/' => 'OE',
            '/ƒ/' => 'f'
        );
        $quotedReplacement = preg_quote($separator, '/');
        $merge = array(
            '/[^\s\p{Zs}\p{Ll}\p{Lm}\p{Lo}\p{Lt}\p{Lu}\p{Nd}]/mu' => ' ',
            '/[\s\p{Zs}]+/mu' => $separator,
            sprintf('/^[%s]+|[%s]+$/', $quotedReplacement, $quotedReplacement) => '',
        );
        $map = $_transliteration + $merge;
        unset($_transliteration);
        return mb_strtolower(preg_replace(array_keys($map), array_values($map), $string));
    }

    public static function persianNumber($input, $mod = 'fa', $colon = '.')
    {
        $num_a = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.');
        $key_a = array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹', $colon);
        return ($mod == 'fa') ? str_replace($num_a, $key_a, $input) : str_replace($key_a, $num_a, $input);
    }

    public static function arabicNumber($input, $mod = 'ar', $colon = '.')
    {
        $num_a = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.');
        $key_a = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩', $colon);
        return ($mod == 'ar') ? str_replace($num_a, $key_a, $input) : str_replace($key_a, $num_a, $input);
    }

    public static function convertToEnNumber($input)
    {
        $input = self::persianNumber($input, 'en');
        $input = self::arabicNumber($input, 'en');
        return $input;
    }

    public static function formatMoney($money, $price = ' تومان')
    {
        return self::formatNumber($money) . $price;
    }

    public static function formatNumber(int $number, bool $thousandSeparator = true, $mod = 'fa')
    {
        return self::persianNumber(number_format($number, 0, '', $thousandSeparator ? ',' : ''), $mod);
    }

    public static function formatDate(Carbon $date, $format = 'Y/n/j - H:i')
    {
        return Jdf::jdate($format, $date->timestamp);
    }

    public static function relativeDateTime(Carbon $datetime)
    {

        $diff = time() - $datetime->timestamp;

        return self::relativeDiff($diff);

    }

    public static function relativeDiff(int $diff)
    {
        $function = function ($diff) {
            if (abs($diff) < 60) {
                return 'همین الان';
            } elseif ($diff > 0) {
                $day_diff = floor($diff / 86400);
                if ($day_diff == 0) {
                    if ($diff < 120) return floor($diff / 60) . ' دقیقه پیش';
                    return floor($diff / 3600) . ' ساعت پیش';
                }
                if ($day_diff == 1) return 'دیروز';
                if ($day_diff == 2) return 'پریروز';
                if ($day_diff < 7) return $day_diff . ' روز پیش';
                if ($day_diff < 31) return ceil($day_diff / 7) . ' هفته پیش';
                if ($day_diff < 60) return 'ماه قبل';

            } else {
                $diff = abs($diff);
                $day_diff = floor($diff / 86400);
                if ($day_diff == 0) {
                    if ($diff < 120) return floor($diff / 60) . ' دقیقه دیگه';
                    return floor($diff / 3600) . ' ساعت دیگه';
                }
                if ($day_diff == 1) return 'فردا';
                if ($day_diff == 2) return 'پسفردا';

                if ($day_diff < 30) return $day_diff . ' روز دیگه';
                if ($day_diff < 60) return 'یک ماه و ' . ($day_diff - 30) . ' روز دیگه';
                if ($day_diff < 365) return intval($day_diff / 30) . ' ماه و ' . ($day_diff - intval($day_diff / 30) * 30) . ' روز دیگه';
                $year = intval($day_diff / 365);
                $month = intval(($day_diff - intval($day_diff / 365) * 365) / 30);
                $day = ($day_diff - intval($day_diff / 30) * 30);
                $output = $year . ' سال ';
                if ($month > 0)
                    $output .= ' و ' . $month . ' ماه ';
                if ($day > 0)
                    $output .= ' و ' . $day . ' روز دیگه';
                return $output;
            }
        };
        return self::persianNumber($function($diff));
    }


    public static function prepareMobile($number)
    {

        $number = $number . '';
        $mapping = ['+' => '', ' ' => ''];
        foreach ($mapping as $key => $value)
            $number = str_replace($key, $value, $number);
        $number = intval($number) . '';
        if (strlen($number) == 10)
            return '98' . $number;

        return $number;
    }

    public static function validateMobile($number)
    {
        $number = self::prepareMobile($number);
        $rules = ['number' => 'required|mobile'];

        $input = compact('number');

        return Validator::make($input, $rules)->passes();
    }

    public static function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }



}