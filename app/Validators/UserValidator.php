<?php
/**
 * Created by PhpStorm.
 * User: roboticsexpert
 * Date: 9/20/17
 * Time: 11:27 AM
 */

namespace App\Validators;


use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class UserValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|min:6',
            'image' => 'image',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name' => 'string|max:255',
            'password' => 'string|min:6',
            'image' => 'image',
            'birthday' => 'date',
        ],
    ];

}